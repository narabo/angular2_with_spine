import { Component } from '@angular/core';
import { Http, Response, Headers, RequestOptions  } from '@angular/http';

@Component({
  selector: 'sign-up',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  editName: string;
  http: Http;
  public status;
  public error;

  save() {
    let name = this.editName;
    let body = JSON.stringify({ name: name });
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.http.post('http://localhost:4300/api/signup', body, options)
      .subscribe(
        res  => {
          this.status = res.status
        },
        error  => {
          this.error = error
        }
      );
  }
}
