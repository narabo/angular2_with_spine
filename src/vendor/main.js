var server = 'http://127.0.0.1:8888';
var socket;
var girl;

enchant();
var ASSETS = {
    girl_png: "assets/walk-animation_0.png",
    enemy_png: "assets/kappe.png"
};
window.onload = function() {
    var load_img = "assets/walk-animation_0.png";
    var game = new Game(800, 400);

    game.fps = 24;
    game.preload(ASSETS.girl_png);
//    socket_init();

    game.onload = function() {
        girl = new Girl();

        // // game.onloadの中に定義すると2回実行されてしまう
        // socket.on("change_sprite", function(data) {
        //     console.log(data.action);
        // });
    };
    game.start();

    var Girl = Class.create(Sprite, {
        initialize : function(){
            Sprite.call(this, 355, 485);
            this.image = game.assets[ASSETS.girl_png];
            this.scale(0.5, 0.5);
            this.frame = Array.from(new Array(17)).map((v, i) => i + 1);
            game.rootScene.addChild(this);
        },
        ontouchstart : function(){
            game.load(ASSETS.enemy_png,function(){
                var enemy = new Enemy(80, 250);
            });
            this.changesprite("walk");
        },
        changesprite : function(action){
            console.log(this.image.methods);
            this.image.parentNode.removeChild(this.image);
            // この辺はspriteが表示される前に実行されるとおかしな挙動になってそう
//            this.parentNode.removeChild(this);
            game.load(ASSETS.enemy_png,function(){
                girl.image = game.assets[ASSETS.enemy_png];
            });
        }
    });

    var Enemy = Class.create(Sprite, {
        initialize : function(x, y){
            Sprite.call(this, 64, 64);
            this.image = game.assets[ASSETS.enemy_png];
            this.scale(1, 1);
            this.frame = Array.from(new Array(17)).map((v, i) => i + 1);
            this.x = x;
            this.y = y;
            game.rootScene.addChild(this);
        },
        ontouchstart : function(){
            this.parentNode.removeChild(this);
        }
    });
}

function socket_init(){
    socket = io.connect(server);
    socket.on("connect", function() {
        console.log("サーバーに接続完了");
    });
    socket.emit("greeting", {message: "HelloWorld!"});
//     socket.on("change_sprite", function(data) {
//        girl.changesprite(data.action);
//     });
}

